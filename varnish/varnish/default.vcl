vcl 4.0;

import std;
import directors;

backend default {
    .host = "localhost";
    .port = "8080";
    .first_byte_timeout = 10s;
    .between_bytes_timeout = 20s;
}

backend client {
    .host = "localhost";
    .port = "3000";
    .first_byte_timeout = 15s;
    .between_bytes_timeout = 15s;
}

backend memsource_api {
    .host = "148.251.31.238";
    .port = "80";
    .first_byte_timeout = 15s;
    .between_bytes_timeout = 15s;
}

sub client_init {
    new client_rr = directors.round_robin();
    client_rr.add_backend(client);
}

sub memsource_api_init {
    new memsource_api_rr = directors.round_robin();
    memsource_api_rr.add_backend(memsource_api);
}

sub backend_recv {
    if (req.url ~ "^(\/client).*$") {
       set req.backend_hint = client_rr.backend();
    }elseif (req.url ~ "^(\/web\/).*$") {
       set req.backend_hint = memsource_api_rr.backend();
    }
    return (pass);
}


########## VARNISH WORK-FLOW SUBROUTINES ##########

sub vcl_init {
    call client_init;
    call memsource_api_init;
}

sub vcl_recv {
    call backend_recv;
    ##Dont cache
    return (pass);
}

sub vcl_pipe {
    set bereq.http.Connection = "close";
    # default logic continues ...
}

sub vcl_pass {
    # default logic continues ...
}

sub vcl_hash {
    # default logic continues ...
}

sub vcl_hit {
    # default logic continues ...
}


sub vcl_miss {
    # default logic continues ...
}

sub vcl_backend_fetch {

}

sub vcl_backend_response {
    return (deliver);
}

# Called before a cached object is delivered to the client.
sub vcl_deliver {
    # default logic continues ...
}

sub vcl_synth {

}

sub vcl_backend_error {
}

