#!/usr/bin/env bash
image="memtest_$1"
sudo docker stop "${image}_1"
sudo docker rm "${image}_1"
sudo docker rmi "${image}"
