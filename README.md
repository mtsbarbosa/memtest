# Mem Test

Hi! This is the **Mem Test** web app powered by Mateus Carvalho
The purpose of this web app is to provide basic access to memsource api


# Pre-requisites

It is designed to run easier on linux (using bash)
If you dont have linux open up.sh file and do same steps in your SO

Some pre-requisites:

 - Maven
 - Java
 - Node.js with npm
 - Docker with docker compose
 - ports 8080, 3306 and 3000 free for use (unless you change configuration files)

## Initial setup

After cloning the repository execute:

    $ sudo docker-compose -d up memtest_mysql

After mysql is up, open src/main/data/migration_0.0.1.sql and execute its content in mysql console
these are default configuration for it:

    jdbc:mysql://127.0.0.1:3306/memtest
    user: data_admin
    password: secret



## Deploying all server side in one

If you have linux simply execute

    sudo sh up.sh
    
    
## Deploying client side
It is currently supporting dev mode only, go to client folder:
npm install
npm run dev

## API documentation
YAML files are in openapi folder



## Addresses
 - Database: jdbc:mysql://127.0.0.1:3306/memtest
 - API Server: http://127.0.0.1:8080
 - Client ui: http://127.0.0.1:3000