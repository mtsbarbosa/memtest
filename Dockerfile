FROM tomcat:latest
ARG rel_n=0.0.1-SNAPSHOT
COPY ./ROOT.xml ./conf/Catalina/localhost/ROOT.xml
COPY ./target/memtest-${rel_n}.war ./webapps/memtest.war
CMD ["catalina.sh", "run"]

EXPOSE 8080
