import _mergeWith from "lodash/mergeWith";
import { Vue } from "vue-property-decorator";

export default class Utils {

    public static isEmpty(obj: any): boolean {

        if (obj == null) {
            return true;
        }

        if (obj.length > 0) {
            return false;
        }
        if (obj.length === 0) {
            return true;
        }

        if (typeof obj !== "object") {
            return false;
        }

        return Object.keys(obj).length === 0 && obj.constructor === Object;
    }

    public static merge(dest, src) {
        _mergeWith(dest, src, function(destValue, srcValue, key) {
            if (srcValue == null) {
                return destValue; // Do not merge properties that are null in src
            }
            if (Array.isArray(destValue)) {
                return Vue.set(dest, key, srcValue);
            }
        });
    }
}
