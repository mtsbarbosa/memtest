/**
 * Removes optional modifier from properties.
 * Opposite of Partial<T> mapped type.
 */
type Complete<T> = {
    [P in keyof T]-?: Complete<T[P]>;
};

export default Complete;
