import Component from "nuxt-class-component";
import Vue from "vue";

@Component
export default class DefaultLayout extends Vue{
  public fixed: boolean = false;
  public right: boolean = true;
  public title: string = "Mem Test";
}
