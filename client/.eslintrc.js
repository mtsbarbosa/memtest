// Default configs: https://github.com/vuejs/eslint-plugin-vue/tree/master/lib/configs
// Rules:           https://github.com/vuejs/eslint-plugin-vue/tree/master/docs/rules

module.exports = {
  env: {
    "node": true,
    "browser": true
  },
  plugins: [
    "vue"
  ],
  extends: [
    'eslint:recommended',
    'plugin:vue/recommended'
  ],
  rules: {
    "vue/html-self-closing": [0, {
      "html": {
        "void": "never",
        "normal": "always",
        "component": "always"
      }
    }],
    "vue/html-indent": [2, 4],
    'vue/attributes-order': [2, {}],
    "vue/max-attributes-per-line": [0, {
      "singleline": 1,
      "multiline": {
        "max": 1,
        "allowFirstLine": false
      }
    }],
    "vue/no-multi-spaces": 2,
    'vue/mustache-interpolation-spacing': [2, 'always'],
    'vue/require-v-for-key': [2],
    'vue/no-unused-vars': [2],
    'vue/v-on-style': [2],
    'vue/html-closing-bracket-newline': 0,
    "no-console": [2, {allow: ["debug"]}]
  }
};
