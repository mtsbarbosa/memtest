import {
  AuthenticationApi,
} from "~/interfaces/memsource";

export default class LoginService {

  private readonly axios;

  constructor(axios) {
    this.axios = axios;
  }

  public getLoginController(): AuthenticationApi {
    console.log("get controller");
    return new AuthenticationApi(
      undefined,
      undefined,
      this.axios,
    );
  }

}
