import {
  UserControllerApi,
} from "~/interfaces/api";

export default class UserService {

  private readonly axios;

  constructor(axios) {
    this.axios = axios;
  }

  public getUserController(): UserControllerApi {
    return new UserControllerApi(
      undefined,
      this.getApiUrl(),
      this.axios,
    );
  }

  private getApiUrl(): string {
    return this.axios.baseURI;
  }

}
