export default function ({ $axios, store }) {
  initInterceptors({$axios, store});
}

export function initInterceptors({ $axios, store }) {
  $axios.onRequest(request => {
    if(store.state.session) {
      if (request.url.endsWith("/connect")) {
        request.withCredentials = true;
        request.setHeader("Cookie", "JSESSIONID=" + store.state.session);
      }
    }
  });

  $axios.onResponse(config => {
  });
}


