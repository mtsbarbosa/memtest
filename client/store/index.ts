import Vuex from "vuex";
import UserModule, { IUserModule } from "~/store/modules/UserModule";
import * as types from "~/store/mutation-types";
import ProjectsModule from "~/store/modules/ProjectsModule";

export interface IStoreState {
  userModule: IUserModule;
}

const store = () => {
    return new Vuex.Store({
        modules: {
            userModule: UserModule,
            projectsModule: ProjectsModule,
        },
    });
};

export default store;
