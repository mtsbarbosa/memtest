import {User, InsertUpdateUser} from "~/interfaces/api";
import UserService from "~/services/UserService";
import { IModuleGetters } from "~/store/IModuleGetters";
import IProcess from "~/interfaces/IProcess";
import * as types from "~/store/mutation-types";
import {isUndefined} from "util";

declare const process: IProcess;

export interface IUserModule {
  user: User;
  insertUpdateUser: InsertUpdateUser,
  session: string;
}

const state: () => IUserModule = () => {
  return {
    user: {},
    insertUpdateUser: {},
    session: "",
  };
};

const getters: IModuleGetters<IUserModule> = {
  user: (state) => {
    return state.user;
  },
  insertUpdateUser: (state) => {
    return state.insertUpdateUser;
  },
  session: (state) => {
    return state.session;
  },
};

const mutations = {
  [types.PATCH_USER](state: IUserModule, user: Partial<User>) {
    state.user = user;
  },
  [types.SET_SESSION](state: IUserModule, session: string) {
    state.session = session;
  },
};

const actions = {
  async [types.FETCH_USER]({ commit, dispatch }) {
    try {
      let data;
      data = (await new UserService(this.$axios).getUserController().getSetupUser()).data;
      commit(types.PATCH_USER, data);
    } catch (error) {
      return false;
    }
  },
  async [types.SUBMIT_USER]({ commit, dispatch }, user: InsertUpdateUser) {
    try {
      let data;
      data = (await new UserService(this.$axios).getUserController().putUsers(user)).data;
      commit(types.PATCH_USER, data);
      dispatch(types.DISPLAY_PROJECTS);
    } catch (error) {
      return false;
    }
  },
  async [types.DISPLAY_SETUP]({ commit, dispatch }) {
    try {
      const path = "/setup";
      if (process.server) {
        this.app.context.redirect(path);
      } else {
        if (this.app.router.currentRoute.path == path) {
          this.app.context.redirect(path);
        } else {
          this.app.router.push(path);
        }
      }
    } catch (error) {
      return false;
    }
  },
  async [types.DISPLAY_PROJECTS]({ commit, dispatch }) {
    try {
      const path = "/projects";
      if (process.server) {
        this.app.context.redirect(path);
      } else {
        if (this.app.router.currentRoute.path == path) {
          this.app.context.redirect(path);
        } else {
          this.app.router.push(path);
        }
      }
    } catch (error) {
      return false;
    }
  },
};

const UserModule = {
  state,
  getters,
  mutations,
  actions,
};

export default UserModule;
