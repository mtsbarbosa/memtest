import {User, InsertUpdateUser} from "~/interfaces/api";
import {LoginDto, LoginResponseDto, AbstractProjectDto, PageDtoAbstractProjectDto} from "~/interfaces/memsource";
import UserService from "~/services/UserService";
import LoginService from "~/services/memsource/LoginService";
import { IModuleGetters } from "~/store/IModuleGetters";
import IProcess from "~/interfaces/IProcess";
import * as types from "~/store/mutation-types";
import {isUndefined} from "util";
import {IUserModule} from "~/store/modules/UserModule";

declare const process: IProcess;

export interface IProjectsModule {
  token: string;
  projects: Array<PageDtoAbstractProjectDto>;
}

const state: () => IProjectsModule = () => {
  return {
    token: "",
    projects: [],
  };
};

const getters: IModuleGetters<IProjectsModule> = {
  token: (state) => {
    return state.token;
  },
  projects: (state) => {
    return state.projects;
  },
};

const mutations = {
  [types.PATCH_PROJECTS](state: IProjectsModule, projects: Array<PageDtoAbstractProjectDto>) {
    state.projects = projects;
  },
  [types.SET_TOKEN](state: IProjectsModule, token: string) {
    state.token = token;
  },
};

const actions = {
  async [types.FETCH_PROJECTS]({ commit, dispatch }, userModule: IUserModule) {
    try {
      let loginData: LoginResponseDto;
      let loginDto: LoginDto = {} as LoginDto;
      console.log('FETCH PROJECTS', userModule);
      loginDto.userName = userModule.user.username? userModule.user.username: "";
      loginDto.password = userModule.user.password? userModule.user.password: "";
      console.log('loginDto',loginDto);
      loginData = (await new LoginService(this.$axios).getLoginController().login(loginDto)) as LoginResponseDto;
      commit(types.SET_TOKEN, loginData.token);
    } catch (error) {
      return false;
    }
  },
};

const ProjectsModule = {
  state,
  getters,
  mutations,
  actions,
};

export default ProjectsModule;
