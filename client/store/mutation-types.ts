export const FETCH_USER = "FETCH_USER";
export const PATCH_USER = "PATCH_USER";
export const SUBMIT_USER = "SUBMIT_USER";
export const DISPLAY_SETUP = "DISPLAY_SETUP";
export const DISPLAY_PROJECTS = "DISPLAY_PROJECTS";

export const SET_TOKEN = "SET_TOKEN";
export const FETCH_PROJECTS = "FETCH_PROJECTS";
export const PATCH_PROJECTS = "PATCH_PROJECTS";


export const SET_SESSION = "SET_SESSION";
