import Component from "nuxt-class-component";
import { Action, Getter, Mutation } from "vuex-class";
import * as types from "~/store/mutation-types";
import Vue from "vue";
import Utils from "~/utils/Utils";
import {InsertUpdateUser} from "~/interfaces/api";

@Component
export default class SetupPageImpl extends Vue {
  @Action(types.SUBMIT_USER) public submitUser;

  public username: string = "";
  public password: string = "";

  public async mounted(){
    await this.$store.dispatch(types.FETCH_USER);
    this.username = this.$store.state.userModule.user.username;
    this.password = this.$store.state.userModule.user.password;
  }

  public async submit() {
    let insertUpdateUser: InsertUpdateUser = {} as InsertUpdateUser;
    insertUpdateUser.username = this.username;
    insertUpdateUser.password = this.password;
    this.submitUser(insertUpdateUser);
  }
}
