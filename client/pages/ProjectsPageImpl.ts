import Component from "nuxt-class-component";
import { Action, Getter, Mutation } from "vuex-class";
import * as types from "~/store/mutation-types";
import Vue from "vue";
import Utils from "~/utils/Utils";
import {InsertUpdateUser} from "~/interfaces/api";

@Component
export default class ProjectsPageImpl extends Vue {
  public async fetch({ store, app: { context: { req } } }): Promise<void> {
    store.dispatch(types.FETCH_PROJECTS, store.state.userModule);
  }

}
