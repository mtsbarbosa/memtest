#!/usr/bin/env bash
echo "Deploying Server"
mvn clean
mvn compile war:war
sudo docker-compose up -d
echo "Server is deployed"
