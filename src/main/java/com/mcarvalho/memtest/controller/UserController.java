package com.mcarvalho.memtest.controller;

import com.mcarvalho.memtest.lang.NotFoundException;
import com.mcarvalho.memtest.model.User;
import com.mcarvalho.memtest.model.dto.UpdateUserDTO;
import com.mcarvalho.memtest.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Optional;

@RestController
public class UserController {
	@Autowired
	private UserService userService;

	@GetMapping("/users/setup")
	public Optional<User> getSetupUser() {
		return userService.findSetupUser();
	}

	@PostMapping("/users/setup")
	public ResponseEntity<User> postUser(@Valid @RequestBody User user) {
		user = userService.save(user);
		return ResponseEntity.ok(user);
	}

	@PutMapping("/users/setup")
	public ResponseEntity<User> putUser(@Valid @RequestBody UpdateUserDTO user) {
		User savedUser;
		try{
			savedUser = userService.update(user);
		}catch (NotFoundException e){
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(savedUser);
	}
}
