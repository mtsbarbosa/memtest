package com.mcarvalho.memtest;

import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WebConfig {
	@Bean
	public ServletListenerRegistrationBean<RestHttpSessionListener> sessionCountListener() {
		ServletListenerRegistrationBean<RestHttpSessionListener> listenerRegBean = new ServletListenerRegistrationBean<>();
		listenerRegBean.setListener(new RestHttpSessionListener());
		return listenerRegBean;
	}
}