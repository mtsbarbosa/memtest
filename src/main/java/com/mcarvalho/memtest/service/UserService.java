package com.mcarvalho.memtest.service;

import com.mcarvalho.memtest.lang.NotFoundException;
import com.mcarvalho.memtest.model.User;
import com.mcarvalho.memtest.model.dto.UpdateUserDTO;
import com.mcarvalho.memtest.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class UserService {
	@Autowired
	private UserRepository userRepository;

	public Optional<User> findSetupUser(){
		return userRepository.findUserBySetupTrue();
	}

	@Transactional(rollbackOn = Exception.class)
	public User save(User user){
		return userRepository.save(user);
	}

	@Transactional(rollbackOn = Exception.class)
	public User update(UpdateUserDTO user) throws NotFoundException{
		Optional<User> optUser = userRepository.findUserBySetupTrue();
		if(!optUser.isPresent()){
			throw new NotFoundException();
		}
		optUser.ifPresent(u -> {
			u.setUsername(user.getUsername());
			u.setPassword(user.getPassword());
			userRepository.save(u);
		});
		return optUser.get();
	}
}
