package com.mcarvalho.memtest.util;

import org.springframework.util.CollectionUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

public class SessionUtil {
	public static final String ATTR_USER = "user";

	public static HttpSession getSession(){
		return getSession(false);
	}

	public static HttpSession getSession(boolean createSession){
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		return attr.getRequest().getSession(createSession);
	}

	public static boolean hasSession(){
		return getSession() != null;
	}
}

