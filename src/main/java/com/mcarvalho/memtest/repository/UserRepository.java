package com.mcarvalho.memtest.repository;

import com.mcarvalho.memtest.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
	public Optional<User> findUserBySetupTrue();
}
