CREATE DATABASE memtest;
CREATE USER 'data_admin'@'%' IDENTIFIED BY 'secret';
FLUSH PRIVILEGES;

CREATE TABLE `user` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`username` VARCHAR(30) NOT NULL,
	`password` VARCHAR(16) NOT NULL,
	`setup` BOOLEAN NOT NULL,
	UNIQUE KEY `idx_username` (`username`) USING BTREE,
	PRIMARY KEY (`id`)
);